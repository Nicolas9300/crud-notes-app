const {Router} = require('express');
const { route } = require('express/lib/application');

const router = new Router();

router.get('/',(req,res)=>{
    res.render('index');
})

router.get('/about',(req,res)=>{
    res.render('about');
})




module.exports = router;