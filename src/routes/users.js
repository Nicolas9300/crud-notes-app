const {Router} = require('express');
const User = require('../models/User');
const passport = require('passport');
const { isAuthenticated } = require('../helpers/auth');

const router = new Router();

router.get('/users/signin',(req,res)=>{
    res.render('users/signin')
});

router.post('/users/signin', passport.authenticate('local',{
    successRedirect: '/notes',
    failureRedirect:'/users/signin',
    failureFlash: true
}));

router.get('/users/signup',(req,res)=>{
    res.render('users/signup')
});

router.post('/users/signup',async (req,res)=>{
  const {name,email,password,confirm_password} =req.body;
  const errors = [];
  if(name <= 0) errors.push({text:'Complete el campo de nombre'});
  if(password != confirm_password) errors.push({text:'Las contraseñas no coinciden'});
  if(password.length < 4) errors.push({text: 'La contraseña tiene que ser de al menos 4 caracteres'});
  if(errors.length > 0){
      res.render('users/signup',{errors,name,email});
  }else{ 
      const emailUser = await User.findOne({email:email});
      if(emailUser){
          req.flash(('error_msg','El email ya esta en uso'));
          res.redirect('/users/signup');
      }
      const newUser = new User({name,email,password});
      newUser.password = await newUser.encryptPassword(password);
      await newUser.save();
      req.flash('success_msg','Se registro correctamente');
      res.redirect('/users/signin');
    }
});

router.get('/users/logout',isAuthenticated, (req, res,next) => {
    req.logout(function(err) {
      if (err) { return next(err); }
      res.redirect('/');
    });
  });

module.exports = router;